﻿Shader "Custom/MosquitoShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Normal ("Normal (RGB)", 2D) = "white" {}
		_Specular ("Specular (RGB)", 2D) = "white" {}
 		_AlphaTest ("Alpha Test", Range(0,1)) = 0.1
 		_AlphaTest ("Alpha Test", Range(0,1)) = 0.1
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		
		CGPROGRAM
		#pragma surface surf StandardSpecular  alphatest:_AlphaTest

		sampler2D _MainTex;
		sampler2D _Normal;
		sampler2D _Specular;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandardSpecular o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed3 normal = UnpackNormal(tex2D (_Normal, IN.uv_MainTex));
			fixed4 spec = tex2D(_Specular, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Specular = spec.rgb;
			o.Normal = normal.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
