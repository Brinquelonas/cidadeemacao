﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerConfig
{
    public string Name;
    public string Number;
    public Color Color;

    public PlayerConfig(string name, string number, Color color)
    {
        Name = name;
        Number = number;
        Color = color;
    }
}

public class GameConfigs
{
    public static List<PlayerConfig> Players;
}

public class GameConfigManager : MonoBehaviour {

    public List<PlayerSelectionToggle> PlayerToggles;
    public Button OKButton;
    public Button BackButton;
    public PotaTween Animation;
    public string GameSceneName = "Game";
    public Openable Fader;
    public GameObject LoadingText;

    private void Awake()
    {
        OKButton.onClick.AddListener(() => 
        {
            OKButtonClicked();
        });

        BackButton.onClick.AddListener(() => 
        {
            Fader.SetActive(true);
            Animation.Reverse(() => 
            {
                SceneManager.LoadScene("Start");
            });
        });
    }

    private void OKButtonClicked()
    {
        List<PlayerConfig> players = new List<PlayerConfig>();

        for (int i = 0; i < PlayerToggles.Count; i++)
        {
            if (PlayerToggles[i].Toggle.isOn)
            {
                if (string.IsNullOrEmpty(PlayerToggles[i].NameInputField.text))
                    return;
                players.Add(new PlayerConfig(PlayerToggles[i].NameInputField.text, PlayerToggles[i].NumberText.text, PlayerToggles[i].Color));
            }
        }

        if (players.Count <= 0)
            return;

        GameConfigs.Players = players;

        Fader.SetActive(true);
        Animation.Reverse(() =>
        {
            //SceneManager.LoadScene(GameSceneName);
            StartCoroutine(LoadSceneAsync(GameSceneName));
        });
    }

    private IEnumerator LoadSceneAsync(string sceneName)
    {
        if (LoadingText != null)
            LoadingText.SetActive(true);

        var async = SceneManager.LoadSceneAsync(sceneName);

        while (!async.isDone)
            yield return null;
    }
}
