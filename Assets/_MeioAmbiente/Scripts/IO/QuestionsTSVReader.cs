﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum QuestionType
{
    None,
    TXT,
    IMG,
    INT
}

public enum SpaceTextType
{
    None,
    Positive,
    Negative,
    Introduction
}

[System.Serializable]
public struct Question
{
    public List<int> Spaces;
    public QuestionType Type;
    public string Text;
    public List<string> WrongAnswers;
    public string RightAnswer;
    public int NPCIndex;
    public string AudioTag;
    public AudioClip Audio;

    public Question(List<int> spaces, QuestionType type, string text, List<string> wrongAnswers, string rightAnswer, int npcIndex, string audioTag)
    {
        Spaces = spaces;
        Type = type;
        Text = text;
        WrongAnswers = wrongAnswers;
        RightAnswer = rightAnswer;
        NPCIndex = npcIndex;
        AudioTag = audioTag;
        Audio = new AudioClip();
    }
}

[System.Serializable]
public struct SapceText
{
    public int Space;
    public SpaceTextType Type;
    public string Text;
    public string AudioTag;

    public SapceText(int space, SpaceTextType type, string text, string audioTag)
    {
        Space = space;
        Type = type;
        Text = text;
        AudioTag = audioTag;
    }
}

public class QuestionsTSVReader : MonoBehaviour {

    public TextAsset TSV;
    public TextAsset SpaceTextTSV;
    public List<Question> Questions = new List<Question>();
    public List<SapceText> SpaceTexts = new List<SapceText>();

    private static QuestionsTSVReader _instance;
    public static QuestionsTSVReader Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<QuestionsTSVReader>();

            return _instance;
        }
    }

    private void Awake()
    {
        ReadTSV();
        ReadSpaceTSV();
    }

    public void ReadTSV()
    {
        string[] lines = TSV.text.Split(System.Environment.NewLine[0]);

        for (int i = 1; i < lines.Length; i++)
        {
            string[] contents = lines[i].Split("\t"[0]);

            List<int> spaces = null;
            if (!string.IsNullOrEmpty(contents[0].Trim()))
            {
                string[] s = contents[0].Split(',');
                for (int j = 0; j < s.Length; j++)
                {
                    if (spaces == null)
                        spaces = new List<int>();

                    int space;
                    if (int.TryParse(s[j].Trim(), out space))
                        spaces.Add(space);
                    else
                    {
                        Debug.LogWarning(string.Concat("Invalid space [{0}", s[j].Trim()));
                        continue;
                    }

                    //spaces.Add(int.Parse(s[j].Trim()));
                }
            }

            QuestionType type;
            try
            {
                type = (QuestionType)System.Enum.Parse(typeof(QuestionType), contents[1].Trim().ToUpper());
            }
            catch
            {
                Debug.LogWarning(string.Concat("Invalid question type {0}", contents[1].Trim().ToUpper()));
                continue;
            }

            string text = contents[2].Trim();

            List<string> wrongAnswers = new List<string>();
            for (int j = 0; j < 3; j++)
            {
                int index = 3 + j;
                if (!string.IsNullOrEmpty(contents[index].Trim()))
                    wrongAnswers.Add(contents[index].Trim());
            }

            string rightAnswer = contents[6].Trim();

            int npcIndex = Random.Range(1, 7);//int.Parse(contents[8].Trim());

            string audioTag = "";

            Questions.Add(new Question(spaces, type, text, wrongAnswers, rightAnswer, npcIndex, audioTag));
        }
    }

    private void ReadSpaceTSV()
    {
        string[] spaceLines = SpaceTextTSV.text.Split(System.Environment.NewLine[0]);

        for (int i = 1; i < spaceLines.Length; i++)
        {
            string[] contents = spaceLines[i].Split("\t"[0]);

            int space = int.Parse(contents[0].Trim());

            SpaceTextType type = SpaceTextType.None;
            switch (contents[1].Trim())
            {
                case "A":
                    type = SpaceTextType.Positive;
                    break;
                case "E":
                    type = SpaceTextType.Negative;
                    break;
                case "I":
                    type = SpaceTextType.Introduction;
                    break;
                default:
                    break;
            }

            string text = contents[2].Trim();
            string audioTag = contents[3].Trim();

            SpaceTexts.Add(new SapceText(space, type, text, audioTag));
        }
    }

    public List<Question> GetQuestionsFromSpace(int space)
    {
        List<Question> questions = new List<Question>();

        for (int i = 0; i < Questions.Count; i++)
        {
            if ((Questions[i].Spaces == null || Questions[i].Spaces.Count == 0 || Questions[i].Spaces.Contains(space)) && !string.IsNullOrEmpty(Questions[i].RightAnswer))
            //if (Questions[i].Type == QuestionType.INT && !string.IsNullOrEmpty(Questions[i].RightAnswer))
            //if (Questions[i].Type == QuestionType.IMG && !string.IsNullOrEmpty(Questions[i].RightAnswer))
                questions.Add(Questions[i]);
        }

        return questions;
    }

    public List<Question> GetInteractionsFromSpace(int space)
    {
        List<Question> questions = new List<Question>();

        for (int i = 0; i < Questions.Count; i++)
        {
            if ((Questions[i].Spaces == null || Questions[i].Spaces.Count == 0 || Questions[i].Spaces.Contains(space)) && !string.IsNullOrEmpty(Questions[i].RightAnswer))
            if (Questions[i].Type == QuestionType.INT && !string.IsNullOrEmpty(Questions[i].RightAnswer))
                //if (Questions[i].Type == QuestionType.IMG && !string.IsNullOrEmpty(Questions[i].RightAnswer))
                questions.Add(Questions[i]);
        }

        return questions;
    }

    public SapceText GetSpaceText(int space, SpaceTextType type)
    {
        print(space + " " + type);

        for (int i = 0; i < SpaceTexts.Count; i++)
        {
            if (SpaceTexts[i].Space == space && SpaceTexts[i].Type == type)
                return SpaceTexts[i];
        }

        return new SapceText();
    }
}
