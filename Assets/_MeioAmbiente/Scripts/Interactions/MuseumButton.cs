﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MuseumButton : MonoBehaviour {

    public class MuseumButtonEvent : UnityEvent<MuseumButton> { }
    public MuseumButtonEvent OnClick = new MuseumButtonEvent();

    public Image PaintingImage;
    public Image BorderImage;

    private MuseumPainting _painting;
    public MuseumPainting Painting
    {
        get { return _painting; }
        set
        {
            _painting = value;

            SetImage(_painting.Image);
        }
    }

    [SerializeField]
    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();

            return _button;
        }
    }

    private void Start()
    {
        Button.onClick.AddListener(() => OnClick.Invoke(this));
    }

    private void SetImage(Sprite image)
    {
        PaintingImage.sprite = image;
        PaintingImage.SetNativeSize();

        CanvasScaler scaler = GetComponentInParent<CanvasScaler>();

        if (PaintingImage.rectTransform.sizeDelta.x > 600)
            PaintingImage.rectTransform.sizeDelta *= 600 / PaintingImage.rectTransform.sizeDelta.x;
        if (PaintingImage.rectTransform.sizeDelta.y > 600)
            PaintingImage.rectTransform.sizeDelta *= 600 / PaintingImage.rectTransform.sizeDelta.y;

        PaintingImage.rectTransform.localScale = Vector3.one;
        BorderImage.rectTransform.sizeDelta = PaintingImage.rectTransform.sizeDelta * 1.05f;
    }

}
