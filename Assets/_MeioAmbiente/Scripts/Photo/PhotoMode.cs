﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PhotoMode : MonoBehaviour {

    public Button Back;
    public Button Photo;
    public PotaTween PhotoFlash;

    public List<GameObject> ObjectsToHide;

    private EasyAR.ImageTrackerBaseBehaviour _tracker;

    void Start ()
    {
        Screen.orientation = ScreenOrientation.AutoRotation;
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true;
        //Fader.Instance.FadeOut();

        Back.onClick.AddListener(() =>
        {
            /*Back.GetComponent<PotaTween>().Reverse();
            Photo.GetComponent<PotaTween>().Reverse();

            Fader.Instance.FadeIn();

            Invoke("LoadMainMenu", 0.55f);*/

            //GetComponent<CameraController>().CameraTexture.Stop();
        });

        Photo.onClick.AddListener(() =>
        {
            foreach (GameObject obj in ObjectsToHide)
                obj.SetActive(false);

            CameraController.Instance.TakePhoto();
            //SoundManager.Instance.PlaySound("Photo");
            PhotoFlash.SetDelay(0.1f).Play(() =>
            {
                PhotoFlash.SetDelay(0f).Reverse();

                foreach (GameObject obj in ObjectsToHide)
                    obj.SetActive(true);
            });
        });

    }
	
	void Update ()
    {
		
	}

    void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }    
    
}
