﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Spine.Unity;
using TMPro;

public class ARPlayer
{
    public string Name;
    public string Number;
    public Color Color;
    public string Log;
    public int RightAnswers;
    public int WrongAnswers;

    public ARPlayer(string name, string number, Color color)
    {
        Name = name;
        Number = number;
        Color = color;
    }
}

public class QuestionInteraction : MonoBehaviour {

    [System.Serializable]
    public struct InteractionConfig
    {
        public string Tag;
        public Interaction Interaction;
    }

	public Image Balloon;
	public TextMeshProUGUI Question;
	public Button[] Buttons;
	public Button[] ImageButtons;
    public List<NPCController> NPCs = new List<NPCController>();
    public List<InteractionConfig> Interactions = new List<InteractionConfig>();
    public string ResourcesPath = "";

    [Header("Player")]
    public QuizPlayer PlayerPrefab;
    public List<ARPlayer> Players = new List<ARPlayer>();

    private int _playerIndex;

    private ARPlayer _currentPlayer
    {
        get
        {
            return Players[_playerIndex];
        }
    }

    [Header("UI")]
    public GamePlayPlayerSelection PlayerSelection;
    public ReportScroll ReportScroll;
    public SendMailPopup SendMailPopup;
    public AlertPopup AlertPopup;
    public Button EndGameButton;

    //private bool _found = false;
    private Question _currentQuestion;

    private string _log;

    public int Space { get; set; }
    public int CorrectAnswer { get; set; }

    public NPCController CurrentNPC
    {
        get
        {
            int index = Mathf.Clamp(_currentQuestion.NPCIndex - 1, 0, NPCs.Count);
            return NPCs[index];
        }
    }

	protected void Awake()
	{
        gameObject.SetActive(false);

		for (int i = 0; i < Buttons.Length; i++) 
		{
			InitializeButton (i);
		}
        for (int i = 0; i < ImageButtons.Length; i++)
        {
            InitializeImageButton(i);
        }

        if (GameConfigs.Players == null || GameConfigs.Players.Count == 0)
            GameConfigs.Players = new List<PlayerConfig>() { new PlayerConfig("teste", "1", Color.red) };
        //GameConfigs.Players = new List<PlayerConfig>() { new PlayerConfig("teste", "1", Color.red), new PlayerConfig("teste2", "2", Color.blue) };

        for (int i = 0; i < GameConfigs.Players.Count; i++)
        {
            ARPlayer player = new ARPlayer(GameConfigs.Players[i].Name, GameConfigs.Players[i].Number, GameConfigs.Players[i].Color);
            player.Log += player.Name + "\n";

            Players.Add(player);
        }

        _log += "Modo Realidade Aumentada\n\n";

        EndGameButton.onClick.AddListener(EndGameButtonClicked);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            OnHandlerFound(0);
        }
    }

	void InitializeButton(int index)
	{
		Buttons[index].onClick.AddListener(() => Answer (index));
	}

    void InitializeImageButton(int index)
    {
        ImageButtons[index].onClick.AddListener(() => Answer(index));
    }

    void EnableButtons()
	{
        for (int i = 0; i < Buttons.Length; i++)
            Buttons[i].enabled = true;
        for (int i = 0; i < ImageButtons.Length; i++)
            ImageButtons[i].enabled = true;
    }

    void DisableButtons()
	{
		for (int i = 0; i < Buttons.Length; i++) 
			Buttons [i].enabled = false;
        for (int i = 0; i < ImageButtons.Length; i++)
            ImageButtons[i].enabled = false;
    }

    public void OnHandlerFound(int space)
	{
        //if (_found)
        //	return;
        //
        //GetComponent<Canvas>().enabled = true;
        //
        //_found = true;

        if (gameObject.activeSelf)
            return;

        gameObject.SetActive(true);

		EnableButtons ();

        Space = space;
        CorrectAnswer = Random.Range(0, 3);

        List<Question> questions = QuestionsTSVReader.Instance.GetQuestionsFromSpace(Space);
        int questionIndex = Random.Range(0, questions.Count);
        _currentQuestion = questions[questionIndex];

        if (Players.Count > 1)
            Question.text = "Qual jogador caiu nessa casa?";
        else
            Question.text = "";
        Balloon.gameObject.SetActive(false);

        CurrentNPC.gameObject.SetActive(true);
        PlaySpineAnimation ("waiting", true);


        switch (_currentQuestion.Type)
        {
            case QuestionType.None:
                break;
            case QuestionType.TXT:
                InitializeTXTQuestion();
                break;
            case QuestionType.IMG:
                InitializeIMGQuestion();
                break;
            case QuestionType.INT:
                InitializeINTQuestion();
                break;
            default:
                break;
        }

        PotaTween.Create(CurrentNPC.gameObject).Stop();
        PotaTween.Create(CurrentNPC.gameObject).
            SetAlpha(0f, 1f).
            SetDuration(0.5f).
            Play(() => 
            {
                PlaySpineAnimation("question", false , () =>
                {
                    PlaySpineAnimation("waiting", true);
                });
                ShowBalloon(() =>
                {
                    /*PlayAudio(_currentQuestion.Audio, () => 
                    {
                        StartCoroutine(ShowAnswers());
                    });*/

                    if (Players.Count > 1)
                    {
                        PlayerSelection.SetActive(true);
                        PlayerSelection.Initiatize(GameConfigs.Players, SetPlayer);
                    }
                    else
                    {
                        SetPlayer(0);
                    }
                });
            });
	}

    public void SetPlayer(string playerName)
    {
        for (int i = 0; i < Players.Count; i++)
        {
            if (Players[i].Name == playerName)
            {
                SetPlayer(i);
                return;
            }
        }
    }

    public void SetPlayer(int index)
    {
        _playerIndex = index;
        PlayerSelection.SetActive(false);

        InitializeQuestion();
    }

    private void InitializeQuestion()
    {
        Question.text = _currentQuestion.Text;
        _currentPlayer.Log += "\n" + _currentQuestion.Text + "\n";

        PlayAudio(_currentQuestion.Audio, () =>
        {
            StartCoroutine(ShowAnswers());
        });
    }

    public void OnHandlerFoundInteraction(int space)
    {
        //if (_found)
        //	return;
        //
        //GetComponent<Canvas>().enabled = true;
        //
        //_found = true;

        if (gameObject.activeSelf)
            return;

        gameObject.SetActive(true);

        EnableButtons();

        Space = space;
        CorrectAnswer = Random.Range(0, 3);

        List<Question> questions = QuestionsTSVReader.Instance.GetInteractionsFromSpace(Space);
        int questionIndex = Random.Range(0, questions.Count);
        _currentQuestion = questions[questionIndex];

        //Question.text = _currentQuestion.Text;
        Balloon.gameObject.SetActive(false);

        CurrentNPC.gameObject.SetActive(true);
        PlaySpineAnimation("waiting", true);

        InitializeINTQuestion();
        
        PotaTween.Create(CurrentNPC.gameObject).Stop();
        PotaTween.Create(CurrentNPC.gameObject).
            SetAlpha(0f, 1f).
            SetDuration(0.5f).
            Play(() =>
            {
                PlaySpineAnimation("question", false, () =>
                {
                    PlaySpineAnimation("waiting", true);
                });
                ShowBalloon(() =>
                {
                    PlayAudio(_currentQuestion.Audio, () =>
                    {
                        StartCoroutine(ShowAnswers());
                    });
                });
            });
    }

    public void Answer(int answer)
	{
		DisableButtons();

		System.Action callback = () => 
		{
            HideAll();
		};

        _currentPlayer.Log += "Resposta certa: " + _currentQuestion.RightAnswer + "\n";
        _currentPlayer.Log += "Resposta dada: " + Buttons[answer].GetComponentInChildren<TextMeshProUGUI>().text + "\n";

        if (answer == CorrectAnswer)
        {
            PlaySpineAnimation("right", false, callback);
            PlayAudio(null);
            SapceText result = QuestionsTSVReader.Instance.GetSpaceText(Space, SpaceTextType.Positive);
            Question.text = "Parabéns!\n" + result.Text;

            _currentPlayer.RightAnswers++;
            _currentPlayer.Log += "Correto!\n";
        }
		else
        {
			PlaySpineAnimation("wrong", false, callback);
            PlayAudio(null);
            SapceText result = QuestionsTSVReader.Instance.GetSpaceText(Space, SpaceTextType.Negative);
            Question.text = "Que pena...\n" + result.Text;

            _currentPlayer.WrongAnswers++;
            _currentPlayer.Log += "Errado...\n";
        }

        StartCoroutine(HideWrongAnswers());
	}

    private void InitializeTXTQuestion()
    {
        List<string> wrongAnswers = new List<string>(_currentQuestion.WrongAnswers);
        for (int i = 0; i < Buttons.Length; i++)
        {
            if (i == CorrectAnswer)
            {
                Buttons[i].GetComponentInChildren<TextMeshProUGUI>().text = _currentQuestion.RightAnswer;
            }
            else
            {
                int answerIndex = Random.Range(0, wrongAnswers.Count);
                Buttons[i].GetComponentInChildren<TextMeshProUGUI>().text = wrongAnswers[answerIndex];
                wrongAnswers.RemoveAt(answerIndex);
            }
            Buttons[i].gameObject.SetActive(false);
        }
    }

    private void InitializeIMGQuestion()
    {
        List<string> wrongAnswers = new List<string>(_currentQuestion.WrongAnswers);
        for (int i = 0; i < ImageButtons.Length; i++)
        {
            string txt = "";
            if (i == CorrectAnswer)
            {
                txt = _currentQuestion.RightAnswer;
            }
            else
            {
                int answerIndex = Random.Range(0, wrongAnswers.Count);
                txt = wrongAnswers[answerIndex];
                wrongAnswers.RemoveAt(answerIndex);
            }
            Image img = ImageButtons[i].transform.Find("Image").GetComponent<Image>();
            img.sprite = Resources.Load<Sprite>(ResourcesPath + "/" + txt);
            img.SetNativeSize();
            ImageButtons[i].gameObject.SetActive(false);
        }
    }

    private void InitializeINTQuestion()
    {
        for (int i = 0; i < Interactions.Count; i++)
        {
            if (Interactions[i].Tag == _currentQuestion.RightAnswer)
            {
                Interactions[i].Interaction.gameObject.SetActive(true);
                Interactions[i].Interaction.StartInteraction(Space);
                Interactions[i].Interaction.OnFinish.AddListener((b) =>
                {
                    if (b)
                        Answer(CorrectAnswer);
                    else
                        Answer(-1);
                    Interactions[i].Interaction.OnFinish.RemoveAllListeners();
                });

                PotaTween.Create(Interactions[i].Interaction.gameObject).
                    SetAlpha(0f, 1f).
                    SetDuration(0.5f).
                    Play();

                break;
            }
        }
    }

    public void EndGameButtonClicked()
    {
        gameObject.SetActive(true);

        ShowBalloon();
        Question.text = "Quem chegou ao final primeiro?";

        PlayerSelection.SetActive(true);
        PlayerSelection.Initiatize(GameConfigs.Players, (s) =>
        {
            PlayerSelection.SetActive(false);
            EndGame(s);
        });
    }

    public void EndGame(string winningPlayerName)
    {
        for (int i = 0; i < Players.Count; i++)
        {
            if (Players[i].Name == winningPlayerName)
            {
                EndGame(Players[i]);
                return;
            }
        }
    }

    public void EndGame(ARPlayer winningPlayer)
    {
        Balloon.gameObject.SetActive(false);

        CurrentNPC.gameObject.SetActive(true);
        PlaySpineAnimation("right", false);

        PotaTween.Create(CurrentNPC.gameObject).Stop();
        PotaTween.Create(CurrentNPC.gameObject).
        SetAlpha(0f, 1f).
        SetDuration(0.5f).
        Play(() =>
        {
            Question.text = "";
            ShowBalloon(() =>
            {
                //PlaySpineAnimation("right", false);
                PlayAudio(null);
                SapceText result = QuestionsTSVReader.Instance.GetSpaceText(Space, SpaceTextType.Positive);
                //Question.text = "Parabéns!\n" + result.Text;
                Question.text = "O ganhador é: " + winningPlayer.Name + "! Parabéns!\nVocê fez " + winningPlayer.RightAnswers + " pontos!";

                InitializeEndGame();
            });
        });
    }

    private void InitializeEndGame()
    {
        Button btn = Instantiate(Buttons[1], transform);
        btn.gameObject.SetActive(true);
        btn.enabled = true;
        btn.GetComponentInChildren<TextMeshProUGUI>().text = "Relatório";
        //btn.GetComponentInChildren<TextMeshProUGUI>().text = "Voltar à tela inicial";
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(() => {
            //SceneManager.LoadScene(0);

            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Log += "\nTotal de respostas certas: " + Players[i].RightAnswers;
                Players[i].Log += "\nTotal de respostas erradas: " + Players[i].WrongAnswers;
                Players[i].Log += "\n-------------------------------------------------------\n";

                _log += Players[i].Log;
            }

            ReportScroll.SetActive(true);
            ReportScroll.Text.text = _log;

            HideElement(CurrentNPC.gameObject);
            HideElement(Balloon.gameObject);
            HideElement(btn.gameObject);
        });
        PotaTween.Create(btn.gameObject).SetAlpha(0f, 1f).SetDuration(0.1f).Play();
    }

    public void ReportScrollSendMailButtonClicked()
    {
        ReportScroll.SetActive(false);
        SendMailPopup.SetActive(true);
    }

    public void SendMailButtonClicked()
    {
        SendMailPopup.SendMailButtonClicked(_log, () =>
        {
            SendMailPopup.SetActive(false);
            AlertPopup.SetActive(true, 1);
            AlertPopup.Text.text = "Relatório enviado com sucesso! Enviar relatório para outro e-mail?";
            AlertPopup.Buttons[0].onClick.AddListener(() => { SceneManager.LoadScene("Start"); });
            AlertPopup.Buttons[1].onClick.AddListener(() =>
            {
                AlertPopup.SetActive(false);
                SendMailPopup.SetActive(true);
            });
        }, () =>
        {
            SendMailPopup.SetActive(false);
            AlertPopup.SetActive(true, 0);
            AlertPopup.Text.text = "Falha ao enviar o relatório. Verifique sua conexão com a internet e tente novamente.";
            AlertPopup.Buttons[0].onClick.AddListener(() =>
            {
                AlertPopup.SetActive(false);
                SendMailPopup.SetActive(true);
            });
        });
    }

    private void PlayAudio(AudioClip clip, System.Action callback = null)
    {
        if (clip == null)
        {
            if (callback != null) callback();
            CurrentNPC.PlayMouthAnimation(2, () => CurrentNPC.PlayMouthAnimation(CurrentNPC.CurrentAnimationState));
            return;
        }

        AudioSource.PlayClipAtPoint(clip, Vector3.zero);

        if (callback != null)
            StartCoroutine(AudioCallback(clip, callback));
    }

    private void PlaySpineAnimation(string animationName, bool loop, System.Action callback = null)
    {
        CurrentNPC.PlayAnimation(animationName, loop, callback);
    }

    private void ShowBalloon(System.Action callback = null)
    {
        Balloon.gameObject.SetActive(true);

        PotaTween.Create(Balloon.gameObject).Stop();
        PotaTween.Create(Balloon.gameObject).
            SetAlpha(0f, 1f).
            SetScale( new Vector3(0.8f, 0.8f, 1), Vector3.one).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutBack).
            Play(callback);
    }

    private void HideButton(int i)
    {
        PotaTween.Create(Balloon.gameObject).Stop();
        if (_currentQuestion.Type == QuestionType.TXT)
        {
            PotaTween.Create(Buttons[i].gameObject).
                SetAlpha(1f, 0f).
                SetScale(Vector3.one, new Vector3(0.8f, 0.8f, 1)).
                SetDuration(0.5f).
                SetEaseEquation(Ease.Equation.OutSine).
                Play(() => 
                {
                    Buttons[i].gameObject.SetActive(false);
                });
        }
        else if (_currentQuestion.Type == QuestionType.IMG)
        {
            PotaTween.Create(ImageButtons[i].gameObject).
                SetAlpha(1f, 0f).
                SetScale(Vector3.one, new Vector3(0.8f, 0.8f, 1)).
                SetDuration(0.5f).
                SetEaseEquation(Ease.Equation.OutSine).
                Play(() =>
                {
                    Buttons[i].gameObject.SetActive(false);
                });
        }
    }

    private void HideAll()
    {
        HideElement(Balloon.gameObject);
        if (_currentQuestion.Type == QuestionType.TXT)
        {
            HideElement(Buttons[CorrectAnswer].gameObject, () =>
            {
            });
        }
        else if (_currentQuestion.Type == QuestionType.IMG)
        {
            HideElement(ImageButtons[CorrectAnswer].gameObject, () =>
            {
            });
        }
        else
        {
            for (int i = 0; i < Interactions.Count; i++)
            {
                if (Interactions[i].Interaction.gameObject.activeSelf)
                {
                    Interaction interaction = Interactions[i].Interaction;
                    DisableInteraction(interaction);
                }
            }
        }
        HideElement(CurrentNPC.gameObject, () => 
        {
            CurrentNPC.gameObject.SetActive(false);
            //GetComponent<Canvas>().enabled = false;
            //_found = false;
            gameObject.SetActive(false);
        });
    }

    private void DisableInteraction(Interaction interaction)
    {
        HideElement(interaction.gameObject, () =>
        {
            interaction.gameObject.SetActive(false);
        });
    }

    private void HideElement(GameObject element, System.Action callback = null)
    {
        PotaTween.Create(element, 1).Stop();
        PotaTween.Create(element, 1).
            SetAlpha(1f, 0f).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutSine).
            Play(() =>
            {
                if (callback != null)
                    callback();

                element.SetActive(false);
            });
    }

    private IEnumerator ShowAnswers()
    {
        Button[] buttons;
        if (_currentQuestion.Type == QuestionType.TXT)
            buttons = Buttons;
        else if (_currentQuestion.Type == QuestionType.IMG)
            buttons = ImageButtons;
        else
            buttons = new Button[0];

        DisableButtons();

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].gameObject.SetActive(true);

            PotaTween.Create(buttons[i].gameObject).Stop();
            PotaTween.Create(buttons[i].gameObject).
            SetAlpha(0f, 1f).
            SetScale(new Vector3(0.8f, 0.8f, 1), Vector3.one).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutBack).
            Play();

            yield return new WaitForSeconds(0.2f);
        }

        EnableButtons();
    }

    private IEnumerator HideWrongAnswers()
    {
        Button[] buttons;
        if (_currentQuestion.Type == QuestionType.TXT)
            buttons = Buttons;
        else if (_currentQuestion.Type == QuestionType.IMG)
            buttons = ImageButtons;
        else
            buttons = new Button[0];

        for (int i = 0; i < buttons.Length; i++)
        {
            if (i != CorrectAnswer)
            {
                HideButton(i);

                yield return new WaitForSeconds(0.2f);
            }

        }
    }

    private IEnumerator AudioCallback(AudioClip clip, System.Action callback)
    {
        float elapsedTime = 0;

        while(elapsedTime < clip.length)
        {
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        callback();
    }
}
