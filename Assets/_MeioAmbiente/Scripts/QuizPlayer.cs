﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizPlayer : MonoBehaviour {

    public string Name;
    public string Number;
    public Color Color;
    public Text NameText;
    public Text NumberText;

    public int RightAnswers, WrongAnswers;
    public string Log;

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = PotaTween.Get(gameObject);
            if (_tween == null)
                _tween = PotaTween.Create(gameObject).SetAlpha(0f, 1f).SetScale(new Vector3(0, 1, 1), Vector3.one).SetDuration(0.5f).SetEaseEquation(Ease.Equation.InOutBack);

            return _tween;
        }
    }

    public void Initialize(string name, string number, Color color)
    {
        Name = name;
        Color = color;
        NameText.text = name;
        NameText.color = color;
        Number = number;
        NumberText.text = number.ToString();
        NumberText.color = color;
    }

    public void SetActive(bool active)
    {
        if (active)
        {
            gameObject.SetActive(true);
            Tween.Stop();
            Tween.Play();
        }
        else
        {
            Tween.Stop();
            Tween.Reverse(() => 
            {
                gameObject.SetActive(false);
            });
        }
    }
}
