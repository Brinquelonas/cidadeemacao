﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateGlobe : MonoBehaviour
{

    bool hasGrabbedPoint = false;
    Vector3 grabbedPoint;

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (!hasGrabbedPoint)
            {
                hasGrabbedPoint = true;
                grabbedPoint = getTouchedPoint();
            }
            else
            {
                Vector3 targetPoint = getTouchedPoint();
                Quaternion rot = Quaternion.FromToRotation(grabbedPoint, targetPoint);
                transform.localRotation *= rot;
            }
        }
        else hasGrabbedPoint = false;
    }


    Vector3 getTouchedPoint()
    {
        RaycastHit hit; Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit);

        return transform.InverseTransformPoint(hit.point);
    }

    /*public float horizontalSpeed = 8f;
    public float verticalSpeed = 8f;

    private bool _updateGlobe = false;

    void Update()
    {
        if (!_updateGlobe)
            return;

        float h = horizontalSpeed * Input.GetAxis("Mouse X");
        float v = verticalSpeed * Input.GetAxis("Mouse Y");

        transform.Rotate(v, 0, -h, Space.World);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z);
    }

    void OnMouseDown()
    {
        _updateGlobe = true;
    }

    void OnMouseUp()
    {
        _updateGlobe = false;
    }*/
}
