﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ViewConfigManager : MonoBehaviour {

    public Button GiantButton;
    public Button MegaButton;
    public Button BackButton;
    public GameObject LoadingText;

    private void Start()
    {
        GiantButton.onClick.AddListener(() => 
        {
            //SceneManager.LoadScene("View");
            StartCoroutine(LoadSceneAsync("View"));
        });

        MegaButton.onClick.AddListener(() =>
        {
            //SceneManager.LoadScene("ViewMega");
            StartCoroutine(LoadSceneAsync("ViewMega"));
        });

        BackButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene("Start");
        });
    }

    private IEnumerator LoadSceneAsync(string sceneName)
    {
        LoadingText.SetActive(true);
        var async = SceneManager.LoadSceneAsync(sceneName);

        while (!async.isDone)
            yield return null;
    }
}
