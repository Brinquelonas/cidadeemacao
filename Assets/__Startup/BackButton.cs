﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour {

	private Button _button;
	public Button Button
	{
		get {
			if (_button == null)
				_button = GetComponent<Button> ();

			return _button;
		}
	}

	void Awake()
	{
		Button.onClick.AddListener(() => {
			SceneManager.LoadScene("Start");
		});
	}
}
